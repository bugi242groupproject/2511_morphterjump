﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asset.Script
{
    public class AutoMove : MonoBehaviour
    {
        [SerializeField] private float moveAmout;
        private bool isMoveLeft;
        private float moveMin;
        private float moveMax;

        void Start()
        {
            isMoveLeft = true;
            moveMin = transform.position.x - moveAmout;
            moveMax = transform.position.x + moveAmout;
        }

        void Update()
        {
            if (Time.timeScale == 0)
            {
                return;
            }

            if (isMoveLeft)
            {
                if (transform.position.x > moveMin)
                {
                    transform.Translate(-0.01f, 0, 0);
                }
                else
                {
                    isMoveLeft = false;
                }
            }
            else
            {
                if (transform.position.x < moveMax)
                {
                    transform.Translate(0.01f, 0, 0);
                }
                else
                {
                    isMoveLeft = true;
                }
            }
        }
    }
}