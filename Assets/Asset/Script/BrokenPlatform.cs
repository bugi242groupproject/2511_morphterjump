﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asset.Script
{
    public class BrokenPlatform : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D rd;
        [SerializeField] private float jumpForce = 10f;

        public void Start()
        {
            rd = GetComponent<Rigidbody2D>();
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            // platform effector-2d to make player to acrosser the platform
            if (collision.relativeVelocity.y <= 0f && collision.gameObject.CompareTag("Player"))
            {
                Rigidbody2D rb = collision.collider.GetComponent<Rigidbody2D>();
                if (rb != null)
                {
                    Vector2 velocity = rb.velocity; // get the vectoc
                    velocity.y = jumpForce; // modify component of the vector
                    rb.velocity = velocity; // set value back to vector

                    rd.bodyType = RigidbodyType2D.Dynamic;
                    rd.gravityScale = 1;
                    GetComponent<BoxCollider2D>().isTrigger = true;
                    Destroy(gameObject, 1);
                }
            }
        }
    }
}