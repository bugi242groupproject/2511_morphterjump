﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Asset.Script
{
    public class StartFallGenerator : MonoBehaviour
    {
        [SerializeField] private GameObject starFallPrefab;
        [SerializeField] private int numberOfStar;
        [SerializeField] private float levelWidth = 3f;
        [SerializeField] private float minY = 5f;
        [SerializeField] private float maxY = 15f;

        [SerializeField] private GameObject player;
        private float score;
        private Vector3 spawnPosition = new Vector3();

        void Start()
        {
            score = player.transform.position.y;

            for (int i = 0; i < numberOfStar; i++)
            {
                spawnPosition.y += Random.Range(minY, maxY);
                spawnPosition.x = Random.Range(-levelWidth, levelWidth);
                Instantiate(starFallPrefab, spawnPosition, Quaternion.identity);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (Mathf.Abs(score - player.transform.position.y) > 50)
            {
                GenerateStarFall();
                score += 50;
            }
        }

        void GenerateStarFall()
        {

            for (int i = 0; i < numberOfStar; i++)
            {
                spawnPosition.y += Random.Range(minY, maxY);
                spawnPosition.x = Random.Range(-levelWidth, levelWidth);
                Instantiate(starFallPrefab, spawnPosition, Quaternion.identity);
            }

            numberOfStar += 3;
        }
    }
}