﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

namespace Asset.Script
{
    public class HighScoreText : MonoBehaviour
    {
        private Text highScore;

        void OnEnable()
        {
            highScore = GetComponent<Text>();
            highScore.text = "High Score : " + PlayerPrefs.GetInt("highScore").ToString();
        }
    }
}