using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Asset.Script
{
	public class PlatformGenerator : MonoBehaviour
	{
		[SerializeField] private GameObject platformPrefab; // target something to  create
		[SerializeField] private int numberOfPlatforms; // number platform at create
		[SerializeField] private float levelWidth = 3f; // ระยะพื้นที่ของ platform ที่ถูกสร้าง
		[SerializeField] private float minY = .2f; // platform ที่ถูกสร้างในเเกน Y จะห่างกันไม่น้อยกว่า 
		[SerializeField] private float maxY = 1.5f; // platform ที่ถูกสร้างในเเกน Y จะไม่ห่างกันมากกว่า 

		[SerializeField] private GameObject player;
		private float score;
		private Vector3 spawnPosition = new Vector3();

		// Use this for initialization
		void Start()
		{
			score = player.transform.position.y;

			for (int i = 0; i < numberOfPlatforms; i++) // make look to play the game
			{
				spawnPosition.y += Random.Range(minY, maxY); // create xxx in Y-axis level scope
				spawnPosition.x = Random.Range(-levelWidth, levelWidth); // create xxx in X-axis level scope
				Instantiate(platformPrefab, spawnPosition, Quaternion.identity);
			}
		}

		void Update()
		{
			if (Mathf.Abs(score - player.transform.position.y) > 50)
			{
				GeneratePlatform();
				score += 50;
			}
		}

		void GeneratePlatform()
		{

			for (int i = 0; i < numberOfPlatforms; i++) // make look to play the game
			{
				spawnPosition.y += Random.Range(minY, maxY); // create xxx in Y-axis level scope
				spawnPosition.x = Random.Range(-levelWidth, levelWidth); // create xxx in X-axis level scope
				Instantiate(platformPrefab, spawnPosition, Quaternion.identity);
			}
		}
	}
}