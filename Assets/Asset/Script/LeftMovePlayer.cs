﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Asset.Script
{
    public class LeftMovePlayer : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private float speed = 0.02f;
        [SerializeField] private GameObject player;
        [SerializeField] private bool isPressed;

        void Update()
        {
            if (isPressed)
            {
                player.transform.Translate(-speed, 0, 0);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            isPressed = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            isPressed = false;
        }
    }
}