using UnityEngine;

namespace Asset.Script
{
	public class Platform : MonoBehaviour 
	{
		[SerializeField] private float jumpForce = 7f;

		void OnCollisionEnter2D(Collision2D collision)
		{
			if (collision.relativeVelocity.y <= 0f)
			{
				Rigidbody2D rb = collision.collider.GetComponent<Rigidbody2D>();
				// edge collider-2d to Check that it collides
				if (rb != null)
				{
					Vector2 velocity = rb.velocity; // get the vectoc
					velocity.y = jumpForce; // modify component of the vector
					rb.velocity = velocity; // set value back to vector
				}
			}
		}
	}
}

