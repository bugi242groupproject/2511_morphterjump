﻿using UnityEngine;
using UnityEngine.UI;

namespace Asset.Script
{
    public class ScoreCount : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D rb;
        [SerializeField] private float topScore = 0f;
        [SerializeField] public Text scoreText;
        [SerializeField] private Transform target;

        // Start is called before the first frame update
        void Start()
        {
            rb = target.gameObject.GetComponent<Rigidbody2D>();
        }

        // Update is called once per frame
        void Update()
        {
            if (rb.velocity.y > 0 && target.position.y > topScore)
            {
                topScore = target.position.y;
            }

            scoreText.text = "Score: " + Mathf.Round(topScore).ToString();
        }
    }
}