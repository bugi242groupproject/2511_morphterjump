﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Asset.Script
{
    public class Detector : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            Debug.Log(other.tag);
            if (other.CompareTag("platform"))
            {
                Destroy(other.gameObject, 1);
            }
            else if (other.CompareTag("Player"))
            {
                Time.timeScale = 0;
                SceneManager.LoadScene("LossUI");
            }
        }
    }
}