﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Asset.Script
{
    public class StartClick : MonoBehaviour
    {
        public void Play()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene("Main");
        }
    }
}