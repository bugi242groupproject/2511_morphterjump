﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asset.Script
{
    public class ResumeClick : MonoBehaviour
    {
        public void Resume()
        {
            Time.timeScale = 1;
        }
    }
}