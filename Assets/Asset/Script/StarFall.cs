﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Asset.Script
{
    public class StarFall : MonoBehaviour
    {
        [SerializeField] private float gravity = -20.10f;
        [SerializeField] private Transform targetPlayer;
        [SerializeField] private float infRange;
        [SerializeField] private float intensity;
        [SerializeField] float disToPlayer;
        private Vector2 pullForce;
        
        private Rigidbody2D rb;
        private void Start()
        {
            rb = GetComponent<Rigidbody2D>();
        }

        private void FixedUpdate()
        {
            StarFall[] dummy = FindObjectsOfType<StarFall>();
            targetPlayer.GetComponent<Player>();

            disToPlayer = Vector2.Distance(targetPlayer.position, transform.position);
            if (disToPlayer <= infRange)
            {
                pullForce = (transform.position - targetPlayer.position).normalized / disToPlayer * intensity;
                rb.AddForce(pullForce, ForceMode2D.Force);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (CompareTag("Player"))
            {
                Time.timeScale = 0;
                SceneManager.LoadScene("LossUI");
            }
        }
    }
}