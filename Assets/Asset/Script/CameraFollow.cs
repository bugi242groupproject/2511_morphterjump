using UnityEngine;

namespace Asset.Script
{
	public class CameraFollow : MonoBehaviour
	{
		// camera follow the player
		[SerializeField] private Transform target;

		void LateUpdate() // update after
		{
			// when y-axis increase, focus to update camera tofollow the player
			if (target.position.y > transform.position.y)
			{
				// create new locate to replace that location before
				Vector3 newPos = new Vector3(transform.position.x, target.position.y, transform.position.z);
				// make camera smooth
				transform.position = newPos;
			}
		}
	}
}