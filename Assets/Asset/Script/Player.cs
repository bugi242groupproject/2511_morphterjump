using UnityEngine;

namespace Asset.Script
{
	[RequireComponent(typeof(Rigidbody2D))]
	public class Player : MonoBehaviour
	{
		private Rigidbody2D rb;
		[SerializeField] private float movementSpeed = 10f;
		[SerializeField] private float movement = 0f;

		void Start()
		{
			Application.targetFrameRate = 60;
			rb = GetComponent<Rigidbody2D>();
		}

		void Update()
		{
			// movement
			movement = Input.GetAxis("Horizontal") * movementSpeed;
		}

		void FixedUpdate()
		{
			Vector2 velocity = rb.velocity; // get the vectoc
			velocity.x = movement; // modify component of the vector
			rb.velocity = velocity; // set value back to vector
		}

	}
}