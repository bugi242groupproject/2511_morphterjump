﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asset.Script
{
    public class PauseClick : MonoBehaviour
    {
        public void Pause()
        {
            Time.timeScale = 0;
        }
    }
}