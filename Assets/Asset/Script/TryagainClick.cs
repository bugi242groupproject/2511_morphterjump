﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Asset.Script
{
    public class TryagainClick : MonoBehaviour
    {
        public void Play()
        {
            SceneManager.LoadScene("StartUI");
        }
    }
}